import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls
import QtQuick.Layouts

Window {
    width: 360
    height: 520
    visible: true
    title: qsTr("Pop quiz")

    GridLayout {
        id: playerInfo
        columns: 2
        x:parent.width * 0.5
        y:parent.height * 0.05

        Label {
            id : player_name_lbl
            font.pixelSize: 16
            text: "Player"
        }
        Label {
            id : current_player_lbl
            font.pixelSize: 16
            color: "green"
            text: "Dummy"
        }
    }

    Label {
        id : active_question_lbl
        y : parent.height*0.2
        font.pixelSize: 16
        text: "How do you feel"
        anchors.horizontalCenter: parent.horizontalCenter
    }

    GroupBox {
        id : questionButtons
        title: "Choose wisely and fast"
        x: parent.width * 0.2
        y: parent.height * 0.3

        GridLayout {
            id: answerButtonGridLayout
            rows: 2
            flow: GridLayout.TopToBottom
            anchors.fill: parent
            Layout.fillHeight: true
            Layout.fillWidth: true

            Button {
                id : nw_answer_btn
                Layout.preferredHeight: 100
                Layout.preferredWidth: 100
                text: "NW"
            }


            Button {
                id : sw_answer_btn
                Layout.preferredHeight: 100
                Layout.preferredWidth: 100
                text: "SW"
            }

            Button {
                id : ne_answer_btn
                Layout.preferredHeight: 100
                Layout.preferredWidth: 100
                text: "NE"
            }

            Button {
                id : se_answer_btn
                Layout.preferredHeight: 100
                Layout.preferredWidth: 100
                text: "SE"
            }
        }
    }

    Column {
        id: status_info
        spacing: 2
        width: parent.width
        y:parent.height*0.8

        Label {
            id : points_label
            font.pixelSize: 16
            text: "Points: 1000"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            id : time_left_lbl
            font.pixelSize: 12
            text: "Time left"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        ProgressBar {
            id : answering_time_bar
//            x: parent.width * 0.1
//            y: parent.height * 0.9
            value: 0.8
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }

}
