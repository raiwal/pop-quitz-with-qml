# Description
Migration from earlier made Quiz application to QML

# Changes
New layout. Even more simpler :-)


![Layout](pop_quiz_qml.png)

# Author
Rainer Waltzer
